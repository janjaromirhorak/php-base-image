FROM php:7.4-apache

# install packages via apt get
RUN apt-get update -y && DEBIAN_FRONTEND=noninteractive apt-get install -y -q --no-install-recommends libpng-dev curl libcurl4-openssl-dev git zip unzip libfreetype6-dev libjpeg62-turbo-dev libpng-dev libmcrypt-dev libmagickwand-dev mariadb-client libzip-dev zlib1g-dev gnupg mailutils msmtp unzip wget lftp
# install imagick
RUN printf "\n" | pecl install imagick
RUN docker-php-ext-enable imagick

# install php extensions
RUN docker-php-ext-install pdo mysqli pdo_mysql curl zip intl

# prepare apache
RUN a2enmod rewrite && a2enmod ssl && a2enmod headers

COPY ./php-additional.ini /usr/local/etc/php/conf.d/php-additional.ini
COPY ./000-default.conf /etc/apache2/sites-available/000-default.conf
COPY ./msmtprc /etc/msmtprc
COPY ./php-mail.conf /usr/local/etc/php/conf.d/mail.ini

RUN mkdir -p /var/run/apache2
RUN mkdir -p /var/lib/php/sessions && chgrp www-data /var/lib/php/sessions && chmod g+rwx /var/lib/php/sessions

# add composer
COPY --from=composer:2 /usr/bin/composer /usr/bin/composer

# add FTP deployment
#RUN wget --output-document=/var/deployer_source.zip https://github.com/arxeiss/ftp-deployment/archive/custom.zip \
RUN wget --output-document=/var/deployer_source.zip 'https://github.com/arxeiss/ftp-deployment/archive/refs/tags/v3.3.1.zip' \
    && echo "phar.readonly=Off" > /usr/local/etc/php/php.ini \
    && unzip /var/deployer_source.zip -d /var \
    && mv '/var/ftp-deployment-3.3.1' '/var/ftp-deployment-custom' \
    && composer install --working-dir=/var/ftp-deployment-custom --optimize-autoloader --ignore-platform-reqs --prefer-dist --no-dev --no-ansi --no-interaction \
    && php /var/ftp-deployment-custom/generatePhar --compress \
    && mv /var/ftp-deployment-custom/deployment.phar /var/ftp-deployment-custom/deployment \
    && chmod +x /var/ftp-deployment-custom/deployment \
    && ln -s /var/ftp-deployment-custom/deployment /usr/local/bin/deployment \
    && ln -s /var/ftp-deployment-custom/vendor /usr/local/bin/vendor

COPY replaceEnvVars.php /usr/local/bin/phpenvsubst
RUN chmod +x /usr/local/bin/phpenvsubst

# restart apache
RUN service apache2 restart